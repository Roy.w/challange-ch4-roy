const versus = document.getElementById("duel")
const newScore = document.getElementById("score")
const newRock = document.getElementById("batu")
const newPaper = document.getElementById("kertas")
const newScissors = document.getElementById("gunting")

class Game {
    constructor(){
        this.playerName = "PLAYER";
        this.comName = "COM";
        this.winner = "";
    }
    comRan(){
        const option = ["✊🏻","✋🏻","✌🏻"]
        const com = option[Math.floor(Math.random() * option.length)];
        return com;
    }
    hover(){
        if(this.comOption == "✊🏻"){
            return newRock.style.backgroundColor = "#d1c9c5"
        }else if(this.comOption == "✋🏻"){
            return newPaper.style.backgroundColor = "#d1c9c5"
        }else if(this.comOption == "✌🏻"){
            return newScissors.style.backgroundColor = "#d1c9c5"
        }
    }
    operation(){
        if(this.comOption == "✊🏻" && this.playerOption == "paper"){
            return this.winner = this.playerName
        }else if(this.comOption == "✊🏻" && this.playerOption == "scissors"){
            return this.winner = this.comName
        }else if(this.comOption == "✋🏻" && this.playerOption == "rock"){
            return this.winner = this.comName
        }else if(this.comOption == "✋🏻" && this.playerOption == "scissors"){
            return this.winner = this.playerName
        }else if(this.comOption == "✌🏻" && this.playerOption == "rock"){
            return this.winner = this.playerName
        }else if(this.comOption == "✌🏻" && this.playerOption == "paper"){
            return this.winner = this.comName
        }else{
            return this.winner = "DRAW"
        }
    }
    matchResult() {
        if (this.winner != "DRAW") {
            return `${this.winner} WIN`;
        } else {
            return `DRAW`;
        }
    }
}
function pickOption(id){
    const play = new Game();
    play.playerOption = id;
    play.comOption = play.comRan();
    play.operation()
    play.matchResult()
    play.hover()
    console.log("com adalah =", play.comOption)
    console.log("player-1 adalah =", play.playerOption);
    console.log("pemenangnya adalah = ", play.winner)
    console.log("hasil akhir = ", play.matchResult())
    versus.remove();
    const appear = document.createElement("p");
    appear.textContent = `${play.matchResult()}`;
    appear.setAttribute("id", newScore.children.length);
    appear.setAttribute("class","box")
    newScore.append(appear)
}
function refresh(){
    newScore.lastChild.remove()
    newRock.style.backgroundColor = "#9C835F"
    newPaper.style.backgroundColor = "#9C835F"
    newScissors.style.backgroundColor = "#9C835F"
}
